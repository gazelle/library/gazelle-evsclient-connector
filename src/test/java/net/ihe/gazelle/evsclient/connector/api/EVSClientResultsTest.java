package net.ihe.gazelle.evsclient.connector.api;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by epoiseau on 30/03/2016.
 */
public class EVSClientResultsTest {

    @Test
    public void getValidationStatusWithInvalidURL() throws Exception {
        assertEquals("FAILED", EVSClientResults.getValidationStatus("1.3.6.1.4.1.12559.11.11.4.22449","https://gazelle.ihe.net/evs"));
    }

    @Test
    public void getValidationStatusWithMissingTrailingSlash() throws Exception {
        assertEquals("FAILED", EVSClientResults.getValidationStatus("1.3.6.1.4.1.12559.11.11.4.22449","https://gazelle.ihe.net/evs"));
    }

    @Test
    public void getValidationStatusWithTrailingSlash() throws Exception {
        assertEquals("FAILED", EVSClientResults.getValidationStatus("1.3.6.1.4.1.12559.11.11.4.22449","https://gazelle.ihe.net/evs"));
    }
    @Test
    public void getValidationStatusFailed() throws Exception {
        assertEquals("FAILED", EVSClientResults.getValidationStatus("1.3.6.1.4.1.12559.11.11.4.22449","https://gazelle.ihe.net/evs"));
    }

    @Test
    public void getValidationStatusPassed() throws Exception {
        assertEquals("PASSED", EVSClientResults.getValidationStatus("1.3.6.1.4.1.12559.11.11.4.22217","https://gazelle.ihe.net/evs"));
    }

    @Test
    public void getValidationStatusAborted() throws Exception {
        assertEquals("ABORTED", EVSClientResults.getValidationStatus("1.3.6.1.4.1.12559.11.11.4.19539","https://gazelle.ihe.net/evs"));
    }

    @Test
    @Ignore("VALIDATION NOT PERFORMED is not a status anymore")
    public void getValidationStatusValidationNotPerformed() throws Exception {
        assertEquals("VALIDATION NOT PERFORMED", EVSClientResults.getValidationStatus("1.3.6.1.4.1.12559.11.1.2.1.4.351221","https://gazelle.ihe.net/evs"));
    }


    @Test
    public void getValidationDate() throws Exception {
        assertEquals("2/13/25 4:13:00 PM (CET GMT+0100)", EVSClientResults.getValidationDate("1.3.6.1.4.1.12559.11.11.4.22217","https://gazelle.ihe.net/evs"));
    }

    @Test
    public void getValidationPermanentLink() throws Exception {
        assertEquals("https://gazelle.ihe.net/evs/report.seam?oid=1.3.6.1.4.1.12559.11.11.4.22217", EVSClientResults.getValidationPermanentLink("1.3.6.1.4.1.12559.11.11.4.22217","https://gazelle.ihe.net/evs"));
    }

    @Test
    public void getXValidationStatusPassed() throws Exception {
        assertEquals("PASSED", EVSClientResults.getXValidationStatus("1.3.6.1.4.1.12559.11.47.2.5337","https://acceptance.ihe-catalyst.net/evs"));
    }

    @Test
    public void getXValidationDate() throws Exception {
        assertEquals("2025-01-30", EVSClientResults.getXValidationDate("1.3.6.1.4.1.12559.11.47.2.5337","https://acceptance.ihe-catalyst.net/evs"));
    }

    @Test
    public void getXValidationPermanentLink() throws Exception {
        assertEquals("https://acceptance.ihe-catalyst.net/evs/xvalidation/validate/report.seam?oid=1.3.6.1.4.1.12559.11.47.2.5337", EVSClientResults.getXValidationPermanentLink("1.3.6.1.4.1.12559.11.47.2.5337", "https://acceptance.ihe-catalyst.net/evs"));
    }
    @Test
    public void getXValidatorName() throws Exception {
        assertEquals("CROSS_VALIDATEUR_DRIM", EVSClientResults.getXValidatorName("1.3.6.1.4.1.12559.11.25.1.3","https://acceptance.ihe-catalyst.net/evs"));
    }
    @Test
    public void getXValidatorDescription() throws Exception {
        assertEquals("Cross-validation projet DRIM", EVSClientResults.getXValidatorDescription("1.3.6.1.4.1.12559.11.25.1.3","https://acceptance.ihe-catalyst.net/evs"));
    }

    @Test
    public void getXValidatorStatus() throws Exception {
        assertEquals("Available", EVSClientResults.getXValidatorStatus("1.3.6.1.4.1.12559.11.25.1.3","https://acceptance.ihe-catalyst.net/evs"));
    }

    @Test
    public void getLastXValPermanentLinkWithExternalId() throws Exception {
        assertEquals("https://acceptance.ihe-catalyst.net/evs/xvalidation/validate/report.seam?oid=1.3.6.1.4.1.12559.11.36.2.3482", EVSClientResults.getLastXValPermanentLinkWithExternalId("1.3.6.1.4.1.12559.11.36.4.5033.0.0","MCA", "https://acceptance.ihe-catalyst.net/evs"));
    }

    @Test
    public void getLastXValStatusWithExternalId() throws Exception {
        assertEquals("PASSED", EVSClientResults.getLastXValStatusWithExternalId("1.3.6.1.4.1.12559.11.36.4.5033.0.0","MCA", "https://acceptance.ihe-catalyst.net/evs"));
    }

    @Test
    public void getLastResultStatusByExternalId() throws Exception {
        assertEquals("FAILED", EVSClientResults.getLastResultStatusByExternalId("4072521","1.3.6.1.4.1.12559.11.1.6", "https://gazelle.ihe.net/evs"));
    }

    @Test
    public void getLastResultPermanentLinkByExternalId() throws Exception {
        assertEquals("https://gazelle.ihe.net/evs/report.seam?oid=1.3.6.1.4.1.12559.11.11.4.26", EVSClientResults.getLastResultPermanentLinkByExternalId("4072521","1.3.6.1.4.1.12559.11.1.6", "https://gazelle.ihe.net/evs"));
    }

}
