package net.ihe.gazelle.evsclient.connector.api;

import net.ihe.gazelle.evsclient.connector.business.exception.UnexpectedEmptyMessageException;
import net.ihe.gazelle.evsclient.connector.model.EVSClientCrossValidatedObject;
import net.ihe.gazelle.evsclient.connector.model.EVSClientValidatedObject;
import net.ihe.gazelle.evsclient.connector.model.MCAValidatedObject;
import net.ihe.gazelle.evsclient.connector.utils.CheckURL;
import net.ihe.gazelle.evsclient.connector.utils.PartSourceUtils;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.PartSource;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

public final class EVSClientServletConnector {

    private static Logger log = LoggerFactory.getLogger(EVSClientServletConnector.class);

    private EVSClientServletConnector() {
    }

    /**
     * This method send the file to validate to the EVSClient tool and redirect the user to the validate.seam page of EVSClient
     *
     * @param object
     * @param extContext
     * @param evsClientUrlIn
     * @param toolOid
     */
    public static void sendToValidation(EVSClientValidatedObject object, ExternalContext extContext,
                                        String evsClientUrlIn, String toolOid) {
        if (object == null || extContext == null || evsClientUrlIn == null || toolOid == null) {
            log.warn("Cannot forward object to EVSClient, check method parameters");
        } else {
            String evsClientUrl;
            try {
                evsClientUrl = CheckURL.checkUrlForUpload(evsClientUrlIn);
            } catch (IOException e) {
                log.warn("Cannot forward object to EVSClient, URL is not reachable: " + evsClientUrlIn);
                return;
            }
            try {
                StringBuilder url = getEvsValidationUrl(evsClientUrl, object, toolOid);
                HttpServletResponse response = (HttpServletResponse) extContext.getResponse();
                response.sendRedirect(evsClientUrl + url.toString());
            } catch (IOException e) {
                log.error("Cannot access to EVSClient : ", e);
            }
        }
    }

    /**
     * This method send the file to validate to the EVSClient tool (From MCA) and without redirecting the user to the validate.seam page of EVSClient
     *
     * @param object
     * @param evsClientUrlIn
     */
    public static String sendToValidationFromMcaWithoutRedirection(MCAValidatedObject object, String evsClientUrlIn) {
        StringBuilder url = new StringBuilder();
        if (object == null || evsClientUrlIn == null) {
            log.warn("SendToValidationFromMca : Cannot forward object to EVSClient, check method parameters");
        } else {
            String evsClientUrl;
            try {
                evsClientUrl = CheckURL.checkUrlForUpload(evsClientUrlIn);
            } catch (IOException e) {
                log.warn("SendToValidationFromMca : Cannot forward object to EVSClient, URL is not reachable: " + evsClientUrlIn);
                return "";
            }
            try {
                url.append("/");
                url.append(getEvsValidationUrl(evsClientUrl, object, object.getToolOid()));
                addMcaParameterToUrl(object, url);
            } catch (IOException e) {
                log.error("Cannot access to EVSClient : ", e);
            }

        }
        return url.toString();
    }

    private static void addMcaParameterToUrl(MCAValidatedObject object, StringBuilder url) {
        addParameterIfNotNullAndNotEmpty(url, "startOffset", object.getStartOffSet());
        addParameterIfNotNullAndNotEmpty(url, "endOffset", object.getEndOffSet());
        addParameterIfNotNullAndNotEmpty(url, "messageContentAnalyzerOid", object.getMessageContentAnalyserOid());
        addParameterIfNotNullAndNotEmpty(url, "validationType", object.getValidationType());
    }

    private static StringBuilder getEvsValidationUrl(String evsClientUrl, EVSClientValidatedObject object, String toolOid) throws IOException {
        if (!evsClientUrl.endsWith("/")) {
            evsClientUrl = evsClientUrl.concat("/");
        }
        // First send the file to EVSClient
        HttpClient client = new HttpClient();
        PostMethod filePost = new PostMethod(evsClientUrl + "upload");

        Part[] parts = {new FilePart("message", object.getPartSource())};
        filePost.setRequestEntity(new MultipartRequestEntity(parts, filePost.getParams()));
        int status = -1;
        try {
            status = client.executeMethod(filePost);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        // redirect the user to the good page
        if (status == HttpStatus.SC_OK) {
            String key = filePost.getResponseBodyAsString();
            String encodedKey = URLEncoder.encode(key, StandardCharsets.UTF_8.toString());

            StringBuilder url = new StringBuilder();
            url.append("validate.seam?key=");
            url.append(encodedKey);
            addParameterIfNotNullAndNotEmpty(url, "externalId", object.getUniqueKey());
            addParameterIfNotNullAndNotEmpty(url, "proxyType", object.getType());
            addParameterIfNotNullAndNotEmpty(url, "toolOid", toolOid);
            return url;
        }
        return new StringBuilder();
    }

    private static void addParameterIfNotNullAndNotEmpty(StringBuilder url, String parameterName, String value){
        if(value !=null && !value.isEmpty()){
            url.append("&" + parameterName + "=");
            url.append(value);
        }
    }

    private static void addParameterIfNotNullAndNotEmpty(StringBuilder url, String parameterName, int value){
        url.append("&" + parameterName + "=");
        url.append(String.valueOf(value));
    }

    /**
     * This method send the file to x validate to the X validator embedded module in EVSClient and redirect the user to the remoteXValidator.seam page of it
     *
     * @param evsClientCrossValidatedObjectList
     * @param extContext
     * @param evsClientUrlIn
     * @param toolOid
     * @param xvalOid
     * @param externalId
     */

    //Generic method for multiple messages
    public static void sendToXValidation(List<EVSClientCrossValidatedObject> evsClientCrossValidatedObjectList, ExternalContext extContext,
                                         String evsClientUrlIn, String toolOid, String xvalOid, String externalId) throws IOException {

        if (evsClientCrossValidatedObjectList == null || extContext == null || evsClientUrlIn == null || toolOid == null || xvalOid == null) {
            log.warn("Cannot forward object to EVSClient, check method parameters");
        } else {
            String evsClientUrl = CheckURL.checkURLForXValidation(evsClientUrlIn,false);

            // First send files (request + response) to EVSClient
            HttpClient client = new HttpClient();

            PostMethod filePost = new PostMethod(evsClientUrl + "xvalupload");
            // use the type as file name

            Part[] parts = new Part[0];
            try {
                parts = createParts(evsClientCrossValidatedObjectList);
            } catch (UnexpectedEmptyMessageException e) {
                e.printStackTrace();
            }
            filePost.setRequestEntity(new MultipartRequestEntity(parts, filePost.getParams()));
            int status = -1;
            try {
                status = client.executeMethod(filePost);
            } catch (Exception e) {
                log.error("Error null message content or input type : ", e);
            }
            // redirect the user to the good page
            if (status == HttpStatus.SC_OK) {
                String key = filePost.getResponseBodyAsString();
                String encodedKey = URLEncoder.encode(key, StandardCharsets.UTF_8.toString());
                HttpServletResponse response = (HttpServletResponse) extContext.getResponse();
                StringBuilder url = new StringBuilder(evsClientUrl);
                url.append("xvalidation/validate/remoteXValidator.seam");
                url.append("?xvaloid=");
                url.append(xvalOid);
                url.append("&file=");
                url.append(encodedKey);
                url.append("&externalId=");
                url.append(externalId);
                url.append("&toolOid=");
                url.append(toolOid);
                response.sendRedirect(url.toString());
            }
        }
    }

    private static Part[] createParts(List<EVSClientCrossValidatedObject> evsClientValidatedObjectList) throws UnexpectedEmptyMessageException {

        Part[] parts = new Part[evsClientValidatedObjectList.size()];

        for (int index=0 ; index < evsClientValidatedObjectList.size(); index++) {
            try {
                parts[index] = createFilePart(PartSourceUtils.buildEncoded64PartSource(evsClientValidatedObjectList.get(index).getMessageContent(),
                        evsClientValidatedObjectList.get(index).getType()), evsClientValidatedObjectList.get(index).getInputType());
            } catch(NullPointerException e){
                log.error("Error null message content or input type : ", e);
                throw new UnexpectedEmptyMessageException("Error null message content or input type ", e);
            }
        }
        return parts;
    }

    private static Part createFilePart(PartSource partSource, String inputType) {
        return new FilePart(inputType, partSource);
    }

    /**
     * This method send the file to x validate to the X validator embedded module in EVSClient and redirect the user to the remoteXValidator.seam page of it
     * It is only for the proxy
     *
     * @param parts
     * @param extContext
     * @param evsClientUrlIn
     * @param toolOid
     * @param messageId
     */

    //Specific method for proxy
    public static void sendToXValidationFromProxy(Part[] parts, ExternalContext extContext,
                                                  String evsClientUrlIn, String toolOid, Integer messageId, String messageType) throws IOException {

        if (parts == null || extContext == null || evsClientUrlIn == null || toolOid == null || messageId == null) {
            log.warn("Cannot forward object to EVSClient, check method parameters");
        } else {
            String evsClientUrl = CheckURL.checkURLForXValidation(evsClientUrlIn,false);

            // First send files (request + response) to EVSClient
            HttpClient client = new HttpClient();

            PostMethod filePost = new PostMethod(evsClientUrl + "xvalupload");
            // use the type as file name

            filePost.setRequestEntity(new MultipartRequestEntity(parts, filePost.getParams()));
            int status = -1;
            try {
                status = client.executeMethod(filePost);
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
            // redirect the user to the good page
            if (status == HttpStatus.SC_OK) {
                String key = filePost.getResponseBodyAsString();
                String encodedKey = URLEncoder.encode(key, StandardCharsets.UTF_8.toString());
                HttpServletResponse response = (HttpServletResponse) extContext.getResponse();
                StringBuilder url = new StringBuilder(evsClientUrl);
                url.append("xvalidation/validate/remoteXValidator.seam");
                url.append("?file=");
                url.append(encodedKey);
                url.append("&messageId=");
                url.append(messageId);
                url.append("&toolOid=");
                url.append(toolOid);
                url.append("&type=");
                url.append(messageType);
                response.sendRedirect(url.toString());
            }
        }
    }

}
