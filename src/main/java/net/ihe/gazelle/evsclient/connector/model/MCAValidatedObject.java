package net.ihe.gazelle.evsclient.connector.model;

import org.apache.commons.httpclient.methods.multipart.PartSource;

/**
 * This interface has to be implemented by the entities which are aimed to be sent to the EVSClient for validation, in this
 * way we can access the properties needed by EVSClient without knowing the content of the entity
 *
 * @author aberge
 */
public interface MCAValidatedObject extends EVSClientValidatedObject {

    /**
     * returns an identifier, unique through the tool for this entity, in order to
     * link the results of the validation to this entity
     *
     * @return
     */
    String getMessageContentAnalyserOid();

    int getStartOffSet();

    int getEndOffSet();

    String getValidationType();

    String getToolOid();


}
