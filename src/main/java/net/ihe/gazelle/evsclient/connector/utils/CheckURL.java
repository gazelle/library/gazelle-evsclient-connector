package net.ihe.gazelle.evsclient.connector.utils;


import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static java.net.HttpURLConnection.HTTP_OK;

/**
 * Created by epoiseau on 30/03/2016.
 */
public final class CheckURL {
    private static final String DEFAULT_EVSCLIENT_URL = "https://gazelle.ihe.net/EVSClient" ;
    private static final Integer TIMEOUT_IN_MS = 1000;
    private static final org.slf4j.Logger LOG = org.slf4j.LoggerFactory.getLogger(CheckURL.class);

    private CheckURL(){

    }

    public static String checkUrlForUpload(String urlIn) throws IOException {
        return checkUrl(urlIn, false);
    }

    public static String checkUrlForResults(String urlIn) throws IOException {
        return checkUrl(urlIn, true);
    }

    private static String checkUrl(String urlIn, boolean forResult) throws IOException {
        String validatorUrlIn;
        if (urlIn == null){
            LOG.error("Input url is null, setting to default "+ DEFAULT_EVSCLIENT_URL);
            throw new IOException("Input url is null, cannot access EVSClient");
        } else {
            validatorUrlIn =  urlIn ;
            if (!validatorUrlIn.endsWith("/")) {
                validatorUrlIn = validatorUrlIn.concat("/");
            }
        }
        HttpURLConnection connection;
        try {

            URL url = new URL(validatorUrlIn);

            connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(TIMEOUT_IN_MS);
            connection.setReadTimeout(TIMEOUT_IN_MS);
            connection.setAllowUserInteraction(false);
            connection.setDoOutput(true);
            connection.setRequestMethod("HEAD");
            Integer responseCode = connection.getResponseCode();
            if (responseCode != HTTP_OK) {
                LOG.error("Got error code : " + responseCode.toString());
                throw new IOException("Got error code: " + responseCode.toString() + " for url at " + urlIn);
            }
        } catch (IOException e) {
            LOG.error("URL is not reachable " + validatorUrlIn);
            throw new IOException(validatorUrlIn + " is not reachable", e);
        }


        String validatorUrl = validatorUrlIn;
        if (forResult) {
            validatorUrl = validatorUrl.concat("rest/");
        }

        return validatorUrl;
    }

    public static String checkURLForXValidation(String urlIn, boolean forResult) throws IOException {

       String validatorUrl = checkUrl(urlIn,forResult);

        if (forResult) {
            validatorUrl = validatorUrl.concat("xvalidation/");
        }

        return validatorUrl;


    }
}
