package net.ihe.gazelle.evsclient.connector.api;

import net.ihe.gazelle.evsclient.connector.utils.CheckURL;
import org.apache.commons.codec.binary.Base64;
import org.jboss.resteasy.client.ClientExecutor;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.resteasy.client.core.executors.URLConnectionClientExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response.Status;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public final class EVSClientResults {

    private static final ClientExecutor CLIENT_EXECUTOR = new URLConnectionClientExecutor();
    private static Logger log = LoggerFactory.getLogger(EVSClientResults.class);


    private EVSClientResults() {

    }

    private static String queryEVSClient(final String validatorUrlIn, String restMethod, String oid, String externalId,
                                         String toolOid) {
        String validatorUrl = null;
        try {
            validatorUrl = CheckURL.checkUrlForResults(validatorUrlIn);
        } catch (IOException e) {
            log.error("Exception with validatorUrl", e.getMessage());
        }

        ClientRequest request;

        if (validatorUrl != null) {

            request = new ClientRequest(validatorUrl.concat(restMethod), CLIENT_EXECUTOR);

            if (oid != null) {
                request.queryParameter("oid", oid);
            }
            if (externalId != null) {
                request.queryParameter("externalId", externalId);
            }
            if (toolOid != null) {
                request.queryParameter("toolOid", toolOid);
            }

        } else {
            return null;
        }

        ClientResponse<String> response = null;

        try {
            response = request.get(String.class);

            if (response != null) {
                if (response.getResponseStatus() == Status.OK && response.getEntity() != null && !response.getEntity().isEmpty()) {
                    return response.getEntity();
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        } finally {
            if (response != null) {
                try {
                    response.releaseConnection();
                } catch (Exception e2) {
                    log.debug("Failed to release connection", e2);
                }
            }
        }
    }

    private static String queryEVSClientForXValidation(final String validatorUrlIn, String restMethod, String oid, String externalId,
                                         String toolOid) {
        String validatorUrl = null;
        try {
            validatorUrl = CheckURL.checkURLForXValidation(validatorUrlIn,true);
        } catch (IOException e) {
            log.error("Exception with validatorUrl", e.getMessage());
        }

        ClientRequest request;

        if (validatorUrl != null) {

            request = new ClientRequest(validatorUrl.concat(restMethod), CLIENT_EXECUTOR);

            if (oid != null) {
                request.queryParameter("oid", oid);
            }
            if (externalId != null) {
                request.queryParameter("externalId", externalId);
            }
            if (toolOid != null) {
                request.queryParameter("toolOid", toolOid);
            }

        } else {
            return null;
        }

        ClientResponse<String> response = null;

        try {
            response = request.get(String.class);

            if (response != null) {
                if (response.getResponseStatus() == Status.OK && response.getEntity() != null && !response.getEntity().isEmpty()) {
                    return response.getEntity();
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        } finally {
            if (response != null) {
                try {
                    response.releaseConnection();
                } catch (Exception e2) {
                    log.debug("Failed to release connection", e2);
                }
            }
        }
    }




    /**
     * Queries the EVSClient to retrieve the status of the validation based on result OID
     *
     * @param oid          : result OID (previously received in parametric URL)
     * @param validatorUrl : Base URL of EVSClient (ended with /)
     *
     * @return
     */
    public static String getValidationStatus(String oid, String validatorUrl) {
        return queryEVSClient(validatorUrl, "GetValidationStatus", oid, null, null);
    }

    /**
     * Queries the EVSClient to retrieve the date of the validation based on result OID
     *
     * @param oid          : result OID (previously received in parametric URL)
     * @param validatorUrl : Base URL of EVSClient (ended with /)
     *
     * @return
     */
    public static String getValidationDate(String oid, String validatorUrl) {
        return queryEVSClient(validatorUrl, "GetValidationDate", oid, null, null);
    }

    /**
     * Queries the EVSClient to retrieve the permanent link to the validation based on result OID
     *
     * @param oid          : result OID (previously received in parametric URL)
     * @param validatorUrl : Base URL of EVSClient (ended with /)
     *
     * @return
     */
    public static String getValidationPermanentLink(String oid, String validatorUrl) {
        return queryEVSClient(validatorUrl, "GetValidationPermanentLink", oid, null, null);
    }

    /**
     * Queries the EVSClient to retrieve the status of the last validation result for the object identified by its external Id
     *
     * @param externalId
     * @param toolOid
     * @param validatorUrl
     *
     * @return
     */
    public static String getLastResultStatusByExternalId(String externalId, String toolOid, String validatorUrl) {
        return queryEVSClient(validatorUrl, "GetLastResultStatusByExternalId", null, externalId, toolOid);
    }

    /**
     * Queries the EVSClient to retrieve the status of the last validation result for the object identified by its external Id
     *
     * @param externalId
     * @param toolOid
     * @param validatorUrl
     *
     * @return
     */
    public static String getLastResultPermanentLinkByExternalId(String externalId, String toolOid, String validatorUrl) {
        return queryEVSClient(validatorUrl, "GetValidationPermanentLinkByExternalId", null, externalId, toolOid);
    }

    /**
     * Queries the EVSClient to retrieve the status of the validation based on result OID
     *
     * @param oid          : result OID (previously received in parametric URL)
     * @param xvalidatorUrl : Base URL of EVSClient (ended with /)
     *
     * @return
     */
    public static String getXValidationStatus(String oid, String xvalidatorUrl) {
        return queryEVSClientForXValidation(xvalidatorUrl, "GetXValidationStatus", oid, null, null);
    }

    /**
     * Queries the EVSClient to retrieve the date of the validation based on result OID
     *
     * @param oid          : result OID (previously received in parametric URL)
     * @param xvalidatorUrl : Base URL of EVSClient (ended with /)
     *
     * @return
     */
    public static String getXValidationDate(String oid, String xvalidatorUrl) {
        return queryEVSClientForXValidation(xvalidatorUrl, "GetXValidationDate", oid, null, null);
    }

    /**
     * Queries the EVSClient to retrieve the permanent link to the validation based on result OID
     *
     * @param oid          : result OID (previously received in parametric URL)
     * @param xvalidatorUrl : Base URL of EVSClient (ended with /)
     *
     * @return
     */
    public static String getXValidationPermanentLink(String oid, String xvalidatorUrl) {
        return queryEVSClientForXValidation(xvalidatorUrl, "GetXValidationPermanentLink", oid, null, null);
    }

    /**
     * Queries the EVSClient to retrieve the name of the x validator based on its oid
     *
     * @param oid          : validator OID
     * @param xvalidatorUrl : Base URL of EVSClient (ended with /)
     *
     * @return
     */
    public static String getXValidatorName(String oid, String xvalidatorUrl) {
        return queryEVSClientForXValidation(xvalidatorUrl, "GetXValidatorName", oid, null, null);
    }

    /**
     * Queries the EVSClient to retrieve the description of the x validator based on its oid
     *
     * @param oid          : validator OID
     * @param xvalidatorUrl : Base URL of EVSClient (ended with /)
     *
     * @return
     */
    public static String getXValidatorDescription(String oid, String xvalidatorUrl) {
        return queryEVSClientForXValidation(xvalidatorUrl, "GetXValidatorDescription", oid, null, null);
    }

    /**
     * Queries the EVSClient to retrieve the status of the x validator based on its oid
     *
     * @param oid          : validator OID
     * @param xvalidatorUrl : Base URL of EVSClient (ended with /)
     *
     * @return
     */
    public static String getXValidatorStatus(String oid, String xvalidatorUrl) {
        return queryEVSClientForXValidation(xvalidatorUrl, "GetXValidatorStatus", oid, null, null);
    }
    /**
     * Queries the EVSClient to retrieve the permanent link of the last cross validation result for the object identified by its external Id
     *
     * @param externalId
     * @param toolOid
     * @param validatorUrl
     *
     * @return
     */
    public static String getLastXValPermanentLinkWithExternalId(String externalId, String toolOid, String validatorUrl) {
        return queryEVSClientForXValidation(validatorUrl, "GetLastXValPermanentLinkWithExternalId", null, externalId, toolOid);
    }

    /**
     * Queries the EVSClient to retrieve the status of the last cross validation result for the object identified by its external Id
     *
     * @param externalId
     * @param toolOid
     * @param validatorUrl
     *
     * @return
     */
    public static String getLastXValStatusWithExternalId(String externalId, String toolOid, String validatorUrl) {
        return queryEVSClientForXValidation(validatorUrl, "GetLastXValStatusWithExternalId", null, externalId, toolOid);
    }


    /**
     * Read the parameters from the URL and decode the OID sent by EVSClient
     *
     * @param urlParams
     *
     * @return
     */
    public static String getResultOidFromUrl(Map<String, String> urlParams) {
        if (urlParams != null && (urlParams.containsKey("oid"))) {
            String oid = urlParams.get("oid");
            byte[] decoded;
            decoded = Base64.decodeBase64(oid.getBytes(StandardCharsets.UTF_8));
            return new String(decoded, StandardCharsets.UTF_8);
        } else {
            return null;
        }
    }

    /**
     * Read the parameters from the URL and decode the OID sent by EVSClient, from X Validator module
     *
     * @param urlParams
     *
     * @return
     */
    public static String getXValResultOidFromUrl(Map<String, String> urlParams) {
        if (urlParams != null && (urlParams.containsKey("logoid"))) {
            String oidxval = urlParams.get("logoid");
            byte[] decoded;
            decoded = Base64.decodeBase64(oidxval.getBytes(StandardCharsets.UTF_8));
            return new String(decoded, StandardCharsets.UTF_8);
        } else {
            return null;
        }
    }
}
