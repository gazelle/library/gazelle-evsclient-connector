package net.ihe.gazelle.evsclient.connector.business.exception;

public class UnexpectedEmptyMessageException extends Exception{

    public UnexpectedEmptyMessageException(String message){
        super(message);
    }

    public UnexpectedEmptyMessageException(String message, Exception e){
        super(message, e);
    }
}
