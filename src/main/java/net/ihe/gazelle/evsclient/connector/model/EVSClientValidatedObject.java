package net.ihe.gazelle.evsclient.connector.model;

import org.apache.commons.httpclient.methods.multipart.PartSource;

/**
 * This interface has to be implemented by the entities which are aimed to be sent to the EVSClient for validation, in this
 * way we can access the properties needed by EVSClient without knowing the content of the entity
 *
 * @author aberge
 */
public interface EVSClientValidatedObject {

    /**
     * returns an identifier, unique through the tool for this entity, in order to
     * link the results of the validation to this entity
     *
     * @return
     */
    String getUniqueKey();

    /**
     * Returns a PartSource object built using the content of the message/document to validate
     * content must Base64 encoded
     * PartSourceUtils::buildEncoded64PartSource method can be used for this purpose
     *
     * @return
     */
    PartSource getPartSource();

    /**
     * identifies the type of object sent to EVSClient to filter the validation types
     * might also be used by EVSClient to build the URL for sending back results
     *
     * @return
     */
    String getType();
}
