package net.ihe.gazelle.evsclient.connector.utils;

import org.apache.commons.httpclient.methods.multipart.ByteArrayPartSource;
import org.apache.commons.httpclient.methods.multipart.PartSource;

import javax.xml.bind.DatatypeConverter;

public final class PartSourceUtils {

    private PartSourceUtils() {
    }

    public static PartSource buildEncoded64PartSource(byte[] content, String type) {
        if (content != null && content.length > 0) {
            byte[] bytes = DatatypeConverter.printBase64Binary(content).getBytes();
            return new ByteArrayPartSource(type, bytes);
        } else {
            return null;
        }
    }
}
