package net.ihe.gazelle.evsclient.connector.model;

/**
 * This interface has to be implemented by the entities which are aimed to be sent to the EVSClient for cross validation, in this
 * way we can access the properties needed by the cross validation module (input type, type of file) without knowing the content of the entity
 *
 * @author nbailliet
 */
public interface EVSClientCrossValidatedObject {

    /**
     * returns the input type (either REQUEST, RESPONSE, MANIFEST or ATTACHMENT) to be used
     * by the cross validation module to affect to inputs
     *
     * @return
     */
    String getInputType();

    /**
     * returns the content of the message to be cross validated
     *
     * @return
     */
    byte[] getMessageContent();

    /**
     * identifies the type of object sent to EVSClient to filter the validation types
     * might also be used by EVSClient to build the URL for sending back results
     *
     * @return
     */
    String getType();
}
